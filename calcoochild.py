import calcoo
import sys

class CalcChild(calcoo.Calc):

    def mul(self, op1, op2):
        return op1 * op2

    def div(self, op1, op2):
        try:

            return op1 / op2

        except ZeroDivisionError:
            print("Division by zero is not allowed")
        raise ValueError("Division by zero is not allowed")

if __name__ == "__main__":
    try:
        operand1 = float(sys.argv[1])
        operand2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: first and third arguments should be numbers")
    calculadora2 = CalcChild()
    if sys.argv[2] == "+":
        resultado = calculadora2.add(operand1, operand2)
        print(resultado)
    elif sys.argv[2] == "-":
        resultado2 = calculadora2.sub(operand1, operand2)
        print(resultado2)
    elif sys.argv[2] == "*":
        resultado3 = calculadora2.mul(operand1, operand2)
        print(resultado3)
    elif sys.argv[2] == "/":
        resultado4 = calculadora2.div(operand1, operand2)
        print(resultado4)

