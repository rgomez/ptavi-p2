import sys

class Calc():

    def add(self, op1, op2):
        return op1 + op2

    def sub(self, op1, op2):
        return op1 - op2

if __name__ == "__main__":
    try:
        operand1 = float(sys.argv[1])
        operand2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: first and third arguments should be numbers")
    calculadora = Calc()
    if sys.argv[2] == "+":
        resultado = calculadora.add(operand1, operand2)
        print(resultado)
    elif sys.argv[2] == "-":
        resultado2 = calculadora.sub(operand1, operand2)
        print(resultado2)
    else:
        sys.exit('Operand should be + or -')
