import sys
import calccount

def process_csv(fichero):
    with open(fichero) as f:
        operaciones = f.readlines()

    for lines in operaciones:
        linea = lines.split(",")
        operand = linea[0]
        firstnumber = int(linea[1])

        for value in linea[2:]:
            nnumbers = int(value)
            calculadora3 = calccount.Calc()
            try:
                if operand == "+":
                    firstnumber = float(calculadora3.add(firstnumber, nnumbers))


                elif operand == "-":
                    firstnumber = float(calculadora3.sub(firstnumber, nnumbers))

                elif operand == "*":
                    firstnumber = float(calculadora3.mul(firstnumber, nnumbers))

                elif operand == "/":
                    try:
                        firstnumber = calculadora3.div(firstnumber, nnumbers)
                    except ZeroDivisionError:
                        print("Division by zero is not allowed")

            except ValueError:
                print("Bad format")
        print(firstnumber)

if __name__ == "__main__":
    process_csv(sys.argv[1])